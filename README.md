# SAP HCP MMS Connector

## Description

Connects a Reekoh Instance to SAP-HCP's MMS API Service.

## Configuration

1. Host - The SAP-HCP Host to send data to.
2. Username - The SAP-HCP account Username.
3. Password -  The SAP-HCP Password.
4. Message Type - The Message Type(ID) to use.

These parameters are then injected to the plugin from the platform.

## Sample input data
```
//single input
{
    timestamp: timestamp,
    data: JSON.stringify({
        temp: 20
    })
}
```

OR
```
//bulk input
[
    {
        timestamp: timestamp,
            data: JSON.stringify({
                temp: 20
            })
    },
    {
        timestamp: timestamp,
            data: JSON.stringify({
                temp: 20
            })
    }
]